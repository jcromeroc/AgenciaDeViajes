AgenciaDeViajes
===============



Instalación de la API
===============

1. Bajar el proyecto y en directorio raiz ejecutar el comando: composer update.
2. Modificar según la configuracion de Base de Datos el archivo: app/config/parameters.yml.
2. Ejecutar el script de mysql que se encuentra en la ruta: bd/AgenciaDeViajes.sql.



Uso de la API
===============
!Importante: TODAS LAS PETICIONES SON DE TIPO POST

Ejecutar en la raiz del proyecto el comando: php bin/console server:run.

1. Para agregar un nuevo usuario se ejecuta desde Postman la siguiente dirección: http://127.0.0.1:8000/user/new.
Ejemplo:
http://127.0.0.1:8000/user/new?json={"cedula": "20652933", "nombre":"Julio", "direccion":"Av Lecuna", "telefono":"04167089959"}

2. Para editar un usuario se ejecuta desde Postman la siguiente dirección: http://127.0.0.1:8000/user/edit.
Ejemplo:
http://127.0.0.1:8000/user/edit?json={"cedula" : "20652933", "nombre" : "Julio Romero", "direccion": "Santa Rosalia", "telefono":"04267089959"}

3. Para crear un nuevo viaje se ejecuta desde Postman la siguiente dirección: http://127.0.0.1:8000/travel/new.
Ejemplo:
http://127.0.0.1:8000/travel/new?json={"codigo":"001", "plazas" : "2", "destino" : "Panamá", "origen" : "Venezuela", "precio" : "500"}

4. Para editar un viaje se ejecuta desde Postman la siguiente dirección: http://127.0.0.1:8000/travel/edit.
Ejemplo:
http://127.0.0.1:8000/travel/edit?json={"codigo":"001", "plazas" : "4", "destino" : "Venezuela", "origen" : "Panamá", "precio" : "1000"}

5. Para asignar un viaje a un usuario se ejecuta desde Postman la siguiente dirección: http://127.0.0.1:8000/travelUser/new.
Ejemplo:
http://127.0.0.1:8000/travelUser/new?json={"cedula":"20652933", "codigo":"001"}

6. Para listar los viajes de un usuario se ejecuta desde Postman la siguiente dirección: http://127.0.0.1:8000/travelUser/list.
Ejemplo:
http://127.0.0.1:8000/travelUser/list?json={"cedula" : "20652933"}
