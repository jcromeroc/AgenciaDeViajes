CREATE DATABASE IF NOT EXISTS agencia_viajes;
USE agencia_viajes;

CREATE TABLE user(
	id 			int(255) auto_increment not null,
	cedula		varchar(255),
	nombre 	varchar(255),
	direccion		varchar(255),
	telefono 	varchar(255),
	CONSTRAINT pk_user PRIMARY KEY(id)
)ENGINE=InnoDb;

CREATE TABLE travel(
	id 			int(255) auto_increment not null,
	codigo		varchar(255),
	plazas		varchar(255),
	destino		varchar(255),
	origen		varchar(255),
	precio		varchar(255),
	CONSTRAINT pk_travel PRIMARY KEY(id)
)ENGINE=InnoDb;

CREATE TABLE travelUser(
	id 			int(255) auto_increment not null,
	user_id		int(255) not null,
	travel_id		int(255) not null,
	CONSTRAINT pk_travels_users PRIMARY KEY(id),
	CONSTRAINT fk_users_travels FOREIGN KEY(user_id) REFERENCES user(id),
	CONSTRAINT fk_travels_users FOREIGN KEY(travel_id) REFERENCES travel(id)
)ENGINE=InnoDb;