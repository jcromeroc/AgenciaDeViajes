<?php
namespace AppBundle\Services;

//Clase que se encarga de convertir información a JSON
class Helper
{
    public $manager;

    //Constructor del servicio
    public function __construct($manager)
    {
        $this->manager = $manager;
    }

    public function json($data)
    {
        //Normaliza los JSON
        $normalizers = array(new \Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer());
        $encoders = array("json" => new \Symfony\Component\Serializer\Encoder\JsonEncoder());
        
        //Serializa los datos a JSON
        $serializer = new \Symfony\Component\Serializer\Serializer($normalizers, $encoders);
        $json = $serializer->serialize($data, 'json');

        //Response de HttpFoundation para devolver la información en JSON 
        $response = new \Symfony\Component\HttpFoundation\Response();
        $response->setContent($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}