<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use BackendBundle\Entity\Travel;
use BackendBundle\Entity\User;
use BackendBundle\Entity\Traveluser;
use AppBundle\Services\Helper;

class TravelUserController extends Controller
{
    public function newAction(Request $request)
    {
        // helper para manejar la respuesta JSON
        $helper = $this->get(Helper::class);
        
        //Recoger datos post
        $json = $request->get("json", null);

        if($json != null)
        {
            //decodificar JSON en parámetros
            $parameters = json_decode($json);
            $user_id = (isset($parameters->cedula)) ? $parameters->cedula : null;
            $travel_id = (isset($parameters->codigo)) ? $parameters->codigo : null;

                if($user_id != null && $travel_id != null)
                {
                    // Entity manager
                    $em = $this->getDoctrine()->getManager();

                    //Encontrar el user por cedula
                    $user = $em->getRepository('BackendBundle:User')->findOneBy(array(
                        "cedula" => $user_id
                    ));

                    //Encontrar el viaje por codigo
                    $travel = $em->getRepository('BackendBundle:Travel')->findOneBy(array(
                        "codigo" => $travel_id
                    ));

                    if(count($travel) != 0 && count($user) != 0)
                    {
                        //Crear entidad de viajes según el usuario
                        $travelUser = new Traveluser();
                        $travelUser->setUser($user);
                        $travelUser->setTravel($travel);

                        //Persistir los datos en BD
                        $em->persist($travelUser);
                        $em->flush();
    
                        //Retornar respuesta exitosa
                        $data = array(
                            'status' => 'success',
                            'code' => 200,
                            'msg' => 'TravelUser user created',
                            'data' => $travelUser
                        );
                    }
                    else
                    {
                        //Retornar respuesta de error
                        $data = array(
                            'status' => 'success',
                            'code' => 400,
                            'msg' => 'TravelUser not created, user or travel dont exits'
                        );
                    }
                }
                else
                {
                    //Retornar respuesta de error
                    $data = array(
                        'status' => 'error',
                        'code' => 400,
                        'msg' => 'TravelUser not created, userId or travelId dont exits'
                    );

                }

        }
        else
        {
            //Respuesta cuando el JSON es vacío
            $data = array(
            'status' => 'error',
            'code' => 400,
            'msg' => 'TravelUser not created, parameters failed'
            );
        }

        //Retornar JSON 
        return $helper->json($data);
    }
    
    public function travelsAction(Request $request)
    {
        // helper para manejar la respuesta JSON
        $helper = $this->get(Helper::class);

        //Recoger datos post
        $json = $request->get("json", null);

        if($json != null)
        {
            //decodificar JSON
            $parameters = json_decode($json);

            //Llamada al Entity manager
            $em = $this->getDoctrine()->getManager();
            //Encontrar el usuario a buscar
            $user = $em->getRepository('BackendBundle:User')->findOneBy(array(
                "cedula" => $parameters->cedula
            ));

            if(count($user) != 0)
            {
                //Obteenr el id del usuario
                $userId = $user->getId();
                //Consulta en la BD de los viajes reservados por el usuario
                $dql = "SELECT t FROM BackendBundle:Traveluser t WHERE t.user = {$userId} ORDER BY t.id DESC"; 
                $query = $em->createQuery($dql);

                //Uso del knp paginator para paginar los viajes según el usuario
                $page = $request->query->getInt('page',1);
                $paginator = $this->get('knp_paginator');
                $itemsPerPage = 10;

                $pagination = $paginator->paginate($query, $page, $itemsPerPage);
                $totalItemsCount = $pagination->getTotalItemCount();

                //Retornar respuesta de éxito
                $data = array(
                    'status' => 'success',
                    'code' => 200,
                    'travels' => $pagination
                );
            }
            else
            {
                //Mensaje de error usuario no existe
                $data = array(
                    'status' => 'error',
                    'code' => 400,
                    'msg' => 'User not exist'
                );
            }
        }
        else
        {
            //Mensaje de error parámetros no encontrados
            $data = array(
                'status' => 'error',
                'code' => 400,
                'msg' => 'Parameters failed'
            );
        }

        //Retornar la respuesta JSON
        return $helper->json($data);
    }
}