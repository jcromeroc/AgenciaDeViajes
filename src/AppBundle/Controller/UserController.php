<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use BackendBundle\Entity\User;
use AppBundle\Services\Helper;

class UserController extends Controller
{
    public function newAction(Request $request)
    {
        // helper para manejar la respuesta JSON
        $helper = $this->get(Helper::class);

        //Recoger datos post
        $json = $request->get("json", null);
        $parameters = json_decode($json);

        // array de error por defecto
        $data = array(
            'status' => 'error',
            'code' => 400,
            'msg' => 'User not created'
        );

        if($json != null)
        {
            //Parámetros recibidos del post para ser insertados en BD
            $cedula = (isset($parameters->cedula)) ? $parameters->cedula : null;
            $nombre = (isset($parameters->nombre)) ? $parameters->nombre : null;
            $direccion = (isset($parameters->direccion)) ? $parameters->direccion : null;
            $telefono = (isset($parameters->telefono)) ? $parameters->telefono : null;

            if($nombre != null && $cedula != null )
            {
                //Creación del nuevo usuario
                $user = new User();
                $user->setCedula($cedula);
                $user->setNombre($nombre);
                $user->setDireccion($direccion);
                $user->setTelefono($telefono);

                //Entity manager
                $em = $this->getDoctrine()->getManager();
                
                //Verificar si el usuario no existe en BD
                $issetUser = $em->getRepository('BackendBundle:User')->findBy(array(
                    "cedula" => $cedula
                ));

                if(count($issetUser) == 0)
                {
                    //Persistencia de usuario en BD
                    $em->persist($user);
                    $em->flush();

                    //Mensaje de usuario agregado exitosamente
                    $data = array(
                        'status' => 'success',
                        'code' => 200,
                        'msg' => 'New user created',
                        'user' => $user
                    );
                }
                else
                {
                    //Mensaje de usuario duplicado
                    $data = array(
                        'status' => 'error',
                        'code' => 400,
                        'msg' => 'User duplicated'
                    );
                }
            }
        }
        else
        {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'msg' => 'User not created, parameters failed'
            );
        }

        return $helper->json($data);
    }

    public function editAction(Request $request)
    {
        // helper para manejar la respuesta JSON
        $helper = $this->get(Helper::class);
        $user = null;

        //Entity manager
        $em = $this->getDoctrine()->getManager();

        //Recoger datos post
        $json = $request->get("json", null);
        $parameters = json_decode($json);
    
        // array de error por defecto
        $data = array(
            'status' => 'error',
            'code' => 400,
            'msg' => 'User not update'
        );

        if($json != null && isset($parameters->cedula))
            //conseguir el objeto a actualizar
            $user = $em->getRepository('BackendBundle:User')->findOneBy(array(
                "cedula" => $parameters->cedula
            ));
    
        if($json != null && count($user) != 0)
        {
            // verificar si se cambian los parametros recibidos por los de la BD
            $nombre = (isset($parameters->nombre)) ? $parameters->nombre : $user->getNombre();
            $direccion = (isset($parameters->direccion)) ? $parameters->direccion : $user->getDireccion();
            $telefono = (isset($parameters->telefono)) ? $parameters->telefono : $user->getTelefono();
    
            // seteo de los parametros recibidos
            $user->setNombre($nombre);
            $user->setDireccion($direccion);
            $user->setTelefono($telefono);
                
            // Persistencia de los datos en la BD
            $em->persist($user);
            $em->flush();
    
            //Mensaje de éxito al cambiar datos
            $data = array(
                'status' => 'success',
                'code' => 200,
                'msg' => 'User update',
                'user' => $user
            );
        }
        else
        {
            //Mensaje de error al cambiar datos
            $data = array(
                'status' => 'error',
                'code' => 400,
                'msg' => 'User not exist'
            );
        }

        //retorna respuesta transformada a JSON
        return $helper->json($data);
    }
}