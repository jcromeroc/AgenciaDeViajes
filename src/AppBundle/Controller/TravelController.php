<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use BackendBundle\Entity\Travel;
use AppBundle\Services\Helper;

class TravelController extends Controller
{
    public function newAction(Request $request)
    {
        // helper para manejar la respuesta JSON
        $helper = $this->get(Helper::class);
        
        //Recoger datos post
        $json = $request->get("json", null);

        if($json != null)
        {
            //Decodificar paramentros del JSON
            $parameters = json_decode($json);
            
            //Parámetros recibidos del post para ser insertados en BD
            $codigo = (isset($parameters->codigo)) ? $parameters->codigo : null;
            $plazas = (isset($parameters->plazas)) ? $parameters->plazas : null;
            $destino = (isset($parameters->destino)) ? $parameters->destino : null;
            $origen = (isset($parameters->origen)) ? $parameters->origen : null;
            $precio = (isset($parameters->precio)) ? $parameters->precio : null;

            // crear viaje
            if($codigo != null)
            {
                //Entity manager
                $em = $this->getDoctrine()->getManager();

                //Verificar si el viaje no existe en BD
                $issetUser = $em->getRepository('BackendBundle:Travel')->findBy(array(
                    "codigo" => $codigo
                ));

                if(count($issetUser) == 0)
                {
                    //Creación de la entiadad viaje
                    $travel = new Travel();
                    $travel->setCodigo($codigo);
                    $travel->setPlazas($plazas);
                    $travel->setDestino($destino);
                    $travel->setOrigen($origen);
                    $travel->setPrecio($precio);

                    //Persitencia en la BD del viaje
                    $em->persist($travel);
                    $em->flush();
        
                    //Mensaje de viaje agregado exitosamente
                    $data = array(
                        'status' => 'success',
                        'code' => 200,
                        'msg' => 'New travel created',
                        'travel' => $travel
                    );
                }
                else
                {
                    //Mensaje de viaje duplicado
                    $data = array(
                        'status' => 'error',
                        'code' => 400,
                        'msg' => 'Travel duplicated'
                    );
                }
                
            }
            else
            {
                //Mensaje de viaje no creado por falta de código
                $data = array(
                    'status' => 'error',
                    'code' => 400,
                    'msg' => 'Travel not created, code invalid'
                );

            }

        }
        else
        {
            //Mensaje de viaje no creado por falta de parámetros
            $data = array(
                'status' => 'error',
                'code' => 400,
                'msg' => 'Task not created, parameters failed'
            );
        }
        
        //retorna respuesta transformada a JSON
        return $helper->json($data);
    }  

    public function editAction(Request $request)
    {
        // helper para manejar la respuesta JSON
        $helper = $this->get(Helper::class);
        $travel = null;

        //Entity manager
        $em = $this->getDoctrine()->getManager();

        //Recoger datos post
        $json = $request->get("json", null);
        $parameters = json_decode($json);
    
        // array de error por defecto
        $data = array(
            'status' => 'error',
            'code' => 400,
            'msg' => 'Travel not update'
        );

        if($json != null && isset($parameters->codigo))
            //conseguir el objeto a actualizar
            $travel = $em->getRepository('BackendBundle:Travel')->findOneBy(array(
                "codigo" => $parameters->codigo
            ));
    
        if($json != null && count($travel) != 0)
        {
            // verificar si se cambian los parametros recibidos por los de la BD
            $plazas = (isset($parameters->plazas)) ? $parameters->plazas : $travel->getPlazas();
            $destino = (isset($parameters->destino)) ? $parameters->destino : $travel->getDestino();
            $origen = (isset($parameters->origen)) ? $parameters->origen : $travel->getOrigen();
            $precio = (isset($parameters->precio)) ? $parameters->precio : $travel->getPrecio();
    
            // seteo de los parametros recibidos
            $travel->setPlazas($plazas);
            $travel->setDestino($destino);
            $travel->setOrigen($origen);
            $travel->setPrecio($precio);
                
            // Persistencia de los datos en la BD
            $em->persist($travel);
            $em->flush();
    
            //Mensaje de éxito al cambiar datos
            $data = array(
                'status' => 'success',
                'code' => 200,
                'msg' => 'Travel update',
                'travel' => $travel
            );
        }
        else
        {
            //Mensaje de error al cambiar datos
            $data = array(
                'status' => 'error',
                'code' => 400,
                'msg' => 'Travel not exist'
            );
        }

        //retorna respuesta transformada a JSON
        return $helper->json($data);
    }
}