<?php

namespace BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Travel
 *
 * @ORM\Table(name="travel")
 * @ORM\Entity
 */
class Travel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigo", type="string", length=255, nullable=true)
     */
    private $codigo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="plazas", type="string", length=255, nullable=true)
     */
    private $plazas;

    /**
     * @var string|null
     *
     * @ORM\Column(name="destino", type="string", length=255, nullable=true)
     */
    private $destino;

    /**
     * @var string|null
     *
     * @ORM\Column(name="origen", type="string", length=255, nullable=true)
     */
    private $origen;

    /**
     * @var string|null
     *
     * @ORM\Column(name="precio", type="string", length=255, nullable=true)
     */
    private $precio;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo.
     *
     * @param string|null $codigo
     *
     * @return Travel
     */
    public function setCodigo($codigo = null)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo.
     *
     * @return string|null
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set plazas.
     *
     * @param string|null $plazas
     *
     * @return Travel
     */
    public function setPlazas($plazas = null)
    {
        $this->plazas = $plazas;

        return $this;
    }

    /**
     * Get plazas.
     *
     * @return string|null
     */
    public function getPlazas()
    {
        return $this->plazas;
    }

    /**
     * Set destino.
     *
     * @param string|null $destino
     *
     * @return Travel
     */
    public function setDestino($destino = null)
    {
        $this->destino = $destino;

        return $this;
    }

    /**
     * Get destino.
     *
     * @return string|null
     */
    public function getDestino()
    {
        return $this->destino;
    }

    /**
     * Set origen.
     *
     * @param string|null $origen
     *
     * @return Travel
     */
    public function setOrigen($origen = null)
    {
        $this->origen = $origen;

        return $this;
    }

    /**
     * Get origen.
     *
     * @return string|null
     */
    public function getOrigen()
    {
        return $this->origen;
    }

    /**
     * Set precio.
     *
     * @param string|null $precio
     *
     * @return Travel
     */
    public function setPrecio($precio = null)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio.
     *
     * @return string|null
     */
    public function getPrecio()
    {
        return $this->precio;
    }
}
