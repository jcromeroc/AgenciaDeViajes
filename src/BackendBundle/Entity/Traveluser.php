<?php

namespace BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Traveluser
 *
 * @ORM\Table(name="travelUser", indexes={@ORM\Index(name="fk_users_travels", columns={"user_id"}), @ORM\Index(name="fk_travels_users", columns={"travel_id"})})
 * @ORM\Entity
 */
class Traveluser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Travel
     *
     * @ORM\ManyToOne(targetEntity="Travel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="travel_id", referencedColumnName="id")
     * })
     */
    private $travel;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set travel.
     *
     * @param \BackendBundle\Entity\Travel|null $travel
     *
     * @return Traveluser
     */
    public function setTravel(\BackendBundle\Entity\Travel $travel = null)
    {
        $this->travel = $travel;

        return $this;
    }

    /**
     * Get travel.
     *
     * @return \BackendBundle\Entity\Travel|null
     */
    public function getTravel()
    {
        return $this->travel;
    }

    /**
     * Set user.
     *
     * @param \BackendBundle\Entity\User|null $user
     *
     * @return Traveluser
     */
    public function setUser(\BackendBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \BackendBundle\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }
}
